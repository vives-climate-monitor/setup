#!/bin/bash

# updaten & upgraden
# apt-get update -y >/dev/null 2>&1            # update & upgrade happens with ansible
# apt-get upgrade -y >/dev/null 2>&1
# rfkill unblock wifi
echo "install hostapd"
apt-get install hostapd -y >/dev/null 2>&1
echo "install dnsmasq"
apt-get install dnsmasq -y >/dev/null 2>&1
echo "unmask hostapd"
systemctl unmask hostapd >/dev/null 2>&1
echo "disable hostapd"
systemctl disable hostapd >/dev/null 2>&1
echo "diable dnsmasq"
systemctl disable dnsmasq >/dev/null 2>&1
#download hostapd.conf
echo "download hostapd.conf"
curl -o /etc/hostapd/hostapd.conf https://gitlab.com/vives-climate-monitor/setup/-/raw/master/hotspot/hostapd.conf >/dev/null 2>&1
#escape, search text and replace in hostapd file
echo "escape, search text and replace in hostapd file"
sed -i 's/#DAEMON_CONF=""/DAEMON_CONF="/etc/hostapd/hostapd.conf"/g' /etc/default/hostapd >/dev/null 2>&1
#add some text after newline
echo "add text to dnsmasq.conf"
echo "#
#AutoHotspot Config
#stop DNSmasq from using resolv.conf
no-resolv
#Interface to use
interface=wlan0
bind-interfaces
expand-hosts
server=8.8.8.8
server=1.1.1.1
server=8.8.4.4
dhcp-range=10.0.0.50,10.0.0.150,12h
" >> /etc/dnsmasq.conf
#add some text after newline
echo "add text to dhcpcd.conf"
echo "nohook wpa_supplicant" >> /etc/dhcpcd.conf
echo "make service for autohotspot"
echo "
[Unit]
Description=Automatically generates an internet Hotspot when a valid ssid is not in range
After=multi-user.target
[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/bin/autohotspot
[Install]
WantedBy=multi-user.target
" >> /etc/systemd/system/autohotspot.service # >/dev/null 2>&1
echo "enable autohotspot"
systemctl enable autohotspot.service
#download autohotspot file
echo "download autohotspot file"
curl -o /usr/bin/autohotspot.file https://gitlab.com/vives-climate-monitor/setup/-/raw/master/hotspot/autohotspot >/dev/null 2>&1
mv /usr/bin/autohotspot.file /usr/bin/autohotspot
#make it executable
echo "make autohotspot executable"
chmod +x /usr/bin/autohotspot
#echo 'sudo rfkill unblock wifi'
echo "execute autohotspot"
bash autohotspot
